# ChainBotz NFT Platform

This application runs a Ethereum NFT platform built on solidity Smart Contracts, deployed on a local test net. The whole UX is just ReactJS JavaScript + HTML there are no other backend systems other then the Ethereum Blockchin

Metamask is used to login and interact with the smart contract system

Running the Applicaitons Requires some setup, its docunmented below so hopefully its easy to follow :-) Please inspect the importand files listed below to view Web3 code.

Also this video shows the application neing used
[VIDEO DEMO](https://www.youtube.com/watch?v=LLT4UPiqu8Y&ab_channel=II)

# Important Folders

The following are Web3 + Smart Contract related files of interest

As we are using a React JS framework for nice UX there is some boilerplate, most web3 code is located in the packages described below

- [`contracts/`](./contracts) : ERC721 Smart Contract
- [`migrations`](./migrations) : Smart Contract Migrations
- [`src/common/abis/*`](./src/common/abis) : Smart Contract ABI's for complied and built Smart COntracts
- [`src/pages/Utility/*`](./src/pages/Utility) : ReactJS code that handles interacting with the Web3 API's
- [`test/ChainBotzNft.test.js`](./test/ChainBotzNft.test.js) : Smart Contract Tests using Chai JS testing framework
- [`truffle-config.js`](./truffle-config.js) : Truffle config file

# Run the Application

Install [Metamask](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn) for intertacting with the web application

First we need a few libraries

```
npm install
```

#### Create some sample Ethererum accounts using `ganache-cli`

```
ganache-cli
```

#### Load an account in to Metamask using the private key

# Available Accounts

```(0) 0x8BD0651eb5c6Ff643D7e4cDc57066803f685Ef8E (100 ETH)
(1) 0x4b57E35EfDA3B1f1523DF6439fdc0e62d15980b4 (100 ETH)
....
```

# Private Keys

```(0) 0x1402a5c87ca2078687b69629f4f1d99bc87a40933a8f5fbb27359e44dfa02140
(1) 0x9f92de74630fcf2eb3516043c27b4326e35f59775aa01fef2272702bec5fef84
.....
```

#### Test and deploy the smart contract

Using the chai test framework

```
truffle test
```

#### Deploy the smart contract

```
truffle migrate --reset
```

#### Start the ReactJS App

```
npm run start
```

#### Navigate to http://localhost:3000

Interact with the app by login in with Metamask

# Run web scanner

Skipfish vulnrability scanner

```
skipfish -o app-main http://192.168.1.33:3000/
skipfish -o view-chain-bot http://192.168.1.33:3000/view-chain-bot/0
skipfish -o add-chain-bot http://192.168.1.33:3000/add-chain-bot
```

# Gitlab Security code scans

```
include:
  - template: Security/SAST.gitlab-ci.yml
```

# Sonarqube code quality

```
variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar" # Defines the location of the analysis task cache
  GIT_DEPTH: "0" # Tells git to fetch all the branches of the project, required by the analysis task

sonarcloud-check:
  stage: sonar
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  only:
    - merge_requests
    - master
    - develop

```
