import { combineReducers } from "redux"

// Front
import Layout from "./layout/reducer"

const reducers = combineReducers({
  // public
  Layout,
})

export default reducers
