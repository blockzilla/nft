import React from "react"
import { Redirect } from "react-router-dom"

// Authentication related pages
import Login from "../pages/Authentication/Login"

//  // Inner Authentication
import Login1 from "../pages/AuthenticationInner/Login"

//Pages
import ListChainBots from "../pages/Utility/ListChainBots"
import AddChainBot from "../pages/Utility/AddChainBot"
import ChainBotDetail from "../pages/Utility/ChainBotDetail"

const userRoutes = [
  //Utility
  { path: "/list-chain-bots", component: ListChainBots },
  { path: "/view-chain-bot/:id", component: ChainBotDetail },
  { path: "/add-chain-bot", component: AddChainBot },
  // this route should be at the end of all other routes
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/list-chain-bots" />,
  },
]

const authRoutes = [
  { path: "/login", component: Login },

  // Authentication Inner
  { path: "/pages-login", component: Login1 },
]

export { userRoutes, authRoutes }
