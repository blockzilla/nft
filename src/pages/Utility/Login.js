import React, { useCallback } from "react"

import { Row, Col, CardBody, Card, Container } from "reactstrap"

// Redux
import { Link } from "react-router-dom"

import logo from "assets/images/logo.svg"

export default function Login(props) {
  // use the web3 provider to login to the app with metamask
  const requestAuth = async web3Context => {
    try {
      await props.web3Context.requestAuth()
    } catch (e) {
      console.error(e)
    }
  }

  // handle auth response with metamask
  const requestAccess = useCallback(web3Context => requestAuth(web3Context), [])
  console.log(props)

  return (
    <React.Fragment>
      <div className="home-btn d-none d-sm-block">
        <Link to="/" className="text-dark">
          <i className="fas fa-home h2" />
        </Link>
      </div>
      <div className="account-pages my-5 pt-sm-5">
        <Container>
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={5}>
              <Card className="overflow-hidden">
                <div className="bg-soft-primary">
                  <Row>
                    <Col className="col-7">
                      <div className="text-primary p-4">
                        <h5 className="text-primary">NFT Swap!</h5>
                        <p>Lets get swapping.</p>
                      </div>
                    </Col>
                    <Col className="col-5 align-self-end"></Col>
                  </Row>
                </div>
                <CardBody className="pt-0">
                  <div>
                    <Link to="/">
                      <div className="avatar-md profile-user-wid mb-4">
                        <span className="avatar-title rounded-circle bg-light">
                          <img
                            src={logo}
                            alt=""
                            className="rounded-circle"
                            height="34"
                          />
                        </span>
                      </div>
                    </Link>
                  </div>
                  <div className="p-2">
                    <div className="mt-3">
                      <button
                        className="btn btn-primary btn-block waves-effect waves-light"
                        type="submit"
                        onClick={requestAccess}
                      >
                        Log In Metamask
                      </button>
                    </div>
                  </div>
                </CardBody>
              </Card>
              <div className="mt-5 text-center">
                <p>
                  © {new Date().getFullYear()} Blockzilla. Crafted with{" "}
                  <i className="mdi mdi-heart text-danger" />
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}
