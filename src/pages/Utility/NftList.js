import React from "react"
import { Link } from "react-router-dom"
import { Card, CardBody, Col, Container, Row } from "reactstrap"
import StarRatings from "react-star-ratings"
import { isEmpty } from "lodash"
import Robohash from "react-robohash"

const NftList = props => {
  const { chainBotsList } = props

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Row>
            <Col lg="12">
              <Row className="mb-3">
                <Col xl="4" sm="6">
                  <div className="mt-2">
                    <h5>Chain Bots</h5>
                  </div>
                </Col>
              </Row>
              <Row>
                {!isEmpty(chainBotsList) &&
                  chainBotsList.map((chainBot, key) => (
                    <Col xl="4" sm="6" key={"_col_" + key}>
                      <Card>
                        <CardBody>
                          <div className="product-img position-relative">
                            <Robohash
                              className="img-fluid mx-auto d-block"
                              name={chainBot[0]}
                              type="robot"
                              background="2"
                            />
                          </div>
                          <div className="mt-4 text-center">
                            <h5 className="mb-3 text-truncate">
                              <Link
                                to={"/view-chain-bot/" + key}
                                className="text-dark"
                              >
                                {chainBot[0]}{" "}
                              </Link>
                            </h5>
                            <div className="text-muted mb-3">
                              <b>Skill:</b>{" "}
                              <StarRatings
                                rating={parseInt(chainBot[3])}
                                starRatedColor="#F1B44C"
                                starEmptyColor="#2D363F"
                                numberOfStars={5}
                                name="rating"
                                starDimension="14px"
                                starSpacing="3px"
                              />
                            </div>
                            <h5 className="my-0">
                              <b>Ξ {chainBot[2]}</b>
                            </h5>
                          </div>
                        </CardBody>
                      </Card>
                    </Col>
                  ))}
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default NftList
