import React, { useState, useEffect } from "react"
import PropTypes from "prop-types"
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"

import { Container, Row, Col, Card, CardBody } from "reactstrap"

import Nftabi from "common/abis/ChainBotzNft.json"
import NftList from "./NftList"
import Web3 from "web3"

const Nfts = props => {
  const [chainBotsList, setChainBotsList] = useState([])

  const { web3 } = props

  useEffect(() => {
    loadWeb3()

    loadContractData()
  }, [])

  // load the web3 provider from metamask
  const loadWeb3 = async () => {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
    } else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    } else {
      window.alert(
        "Non-Ethereum browser detected. You should consider trying MetaMask!"
      )
    }
  }

  // load the contract
  const loadContractData = async () => {
    const networkId = await web3.eth.net.getId()
    const networkData = Nftabi.networks[parseInt(networkId)]
    if (networkData) {
      const abi = Nftabi.abi
      const address = networkData.address
      const contract = new web3.eth.Contract(abi, address)

      // get all NFTS from the contract
      const totalSupply = await contract.methods.totalSupply().call()

      console.log(totalSupply)

      // get the name of the NFT
      const nftName = await contract.methods.name().call()

      console.log(nftName)

      let name
      let result = []

      // look through all NFTs and retrieve based on ID
      for (var c = 1; c <= totalSupply; c++) {
        name = await contract.methods.getChainBot(c - 1).call()
        result.push(name)
      }
      console.log(result)

      // store the chainbots in the Redux state for rendering
      setChainBotsList(result)
    }
  }

  return (
    <React.Fragment>
      {chainBotsList && chainBotsList.length > 0 ? (
        <NftList chainBotsList={chainBotsList} />
      ) : (
        <div className="page-content">
          <Container fluid>
            <Row>
              <Col lg="3">
                <Card>
                  <CardBody>
                    <div>
                      There are no Chain Bot NFT&apos;s <br />
                      <br />
                      <Link to={"/add-chain-bot"} className="text-dark">
                        {" "}
                        Create Chain Bot{" "}
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      )}
    </React.Fragment>
  )
}

Nfts.propTypes = {
  history: PropTypes.object,
}

function mapStateToProps(state) {
  return {}
}

const mapDispatchToProps = dispatch => ({})

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Nfts))
