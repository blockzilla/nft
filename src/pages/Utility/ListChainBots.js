import React, { useEffect, useCallback } from "react"

//Import Breadcrumb
import Login from "./Login"
import Nfts from "./Nfts"
import { useWeb3 } from "@openzeppelin/network/react"

import Web3 from "web3"

const ListChainBots = () => {
  // remove infura API token from web app, as anyone with a browser could use it
  // instead use a node which can be whitelisted with CORS to allow requests only
  // from the domain hosting the web app
  const web3Context = useWeb3(`http://localhost:8545`)

  const { networkId, accounts, providerName, lib } = web3Context

  const getBalance = useCallback(async () => {
    console.log("account exists")
  }, [accounts, lib.eth, lib.utils])

  useEffect(() => {
    getBalance()
  }, [accounts, getBalance, networkId])

  useEffect(() => {
    loadWeb3()
  }, [])

  // load the web 3 provider and pas to child componentes
  const loadWeb3 = async () => {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
    } else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    } else {
      window.alert("MetaMask not installed")
    }
  }

  return (
    <React.Fragment>
      {accounts &&
      accounts.length &&
      !!networkId &&
      providerName !== "infura" ? (
        <Nfts web3={window.web3} />
      ) : (
        <Login web3Context={web3Context} />
      )}
    </React.Fragment>
  )
}

export default ListChainBots
