import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { Button, Card, CardBody, Col, Container, Row, Table } from "reactstrap"
import { isEmpty } from "lodash"

//Import Star Ratings
import StarRatings from "react-star-ratings"

//Import Breadcrumb
import Breadcrumbs from "components/Common/Breadcrumb"
import Web3 from "web3"

import Robohash from "react-robohash"
import Nftabi from "common/abis/ChainBotzNft.json"

const ChainBotDetail = props => {
  const [chainBot, setChainBot] = useState([])
  const [account, setAccount] = useState([])
  const [contractAddress, setContractAddress] = useState([])

  useEffect(() => {
    loadWeb3()
    loadContractData()
  }, [])

  // Load web3 provider
  const loadWeb3 = async () => {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
    } else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    } else {
      window.alert("MetaMask not installed")
    }
  }

  // load the smart contract data and call Web3 apis
  const loadContractData = async () => {
    const web3 = window.web3

    // load the smart contract account
    const accounts = await web3.eth.getAccounts()
    setAccount(accounts[0])
    const networkID = await web3.eth.net.getId()
    const networkData = Nftabi.networks[parseInt(networkID)]

    // read the contract
    if (networkData) {
      const contract = new web3.eth.Contract(Nftabi.abi, networkData.address)
      setContractAddress(networkData.address)

      let name
      let result = []

      // set a variable to use for calling the smart contract to get a specific bot
      let param = props.location.pathname.split("/view-chain-bot/")[1]

      // check id for numbers with in the range
      // ensure we only allow numbers
      const testID = /^0*(?:[0-9][0-9]?|100)$/
      var patt = new RegExp(testID)
      try {
        if (param.length > 0 && !isNaN(param) && patt.test(param)) {
          // call the smart contract api
          name = await contract.methods.getChainBot(param).call()
          result.push(name)
          // se the chain bot in the state
          console.log(result[0])
          setChainBot(result[0])
        } else {
          console.error(`invalid param %s`, param)
        }
      } catch (error) {
        console.error(error)
      }
    }
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="Shop" breadcrumbItem="ChainBot Detail" />
          {!isEmpty(chainBot) ? (
            <Row>
              <Col>
                <Card>
                  <CardBody>
                    <Row>
                      <Col xl="6">
                        <div className="product-detai-imgs">
                          <Row>
                            <Col md={{ size: 7, offset: 1 }} xs="9">
                              <Robohash
                                className="img-fluid mx-auto d-block"
                                name={chainBot[0]}
                                type="robot"
                                background="2"
                              />
                              <br />
                              <br />
                              <br />
                              <div className="text-center">
                                <Button
                                  type="button"
                                  color="success"
                                  className="ml-1 btn waves-effect  mt-2 waves-light"
                                >
                                  <i className="bx bx-shopping-bag mr-2" />
                                  Buy now
                                </Button>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>

                      <Col xl="6">
                        <div className="mt-4 mt-xl-3">
                          <Link to="#" className="text-primary">
                            Chain Bot
                          </Link>
                          <h4 className="mt-1 mb-3">{chainBot[0]}</h4>

                          <div className="text-muted float-left mr-3 mb-3">
                            <b>Skill </b>
                            <StarRatings
                              rating={parseInt(chainBot[3])}
                              starRatedColor="#F1B44C"
                              starEmptyColor="#2D363F"
                              numberOfStars={5}
                              name="rating"
                              starDimension="14px"
                              starSpacing="3px"
                            />
                          </div>
                          <br />
                          <br />
                          <br />
                          <h5 className="mb-4">
                            Price : <b>Ξ {chainBot[2]} </b>
                          </h5>
                          <p className="text-muted mb-4">{chainBot[1]}</p>
                          <Row className="mb-6">
                            <Col md="6">
                              <div key="dsa">
                                <p className="text-muted">Created By: NFTWiz</p>
                              </div>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                    </Row>

                    <div className="mt-5">
                      <h5 className="mb-3">Chain Info:</h5>

                      <div className="table-responsive">
                        <Table className="table mb-0 table-bordered">
                          <tbody>
                            <tr key="11a11edasd">
                              <th
                                scope="row"
                                style={{ width: "400px" }}
                                className={"text-capitalize"}
                              >
                                Contract Adddress
                              </th>
                              <td>{contractAddress}</td>
                            </tr>
                            <tr key="1111edasd">
                              <th
                                scope="row"
                                style={{ width: "400px" }}
                                className={"text-capitalize"}
                              >
                                Owned By
                              </th>
                              <td>{account}</td>
                            </tr>
                            <tr key="1111edasad">
                              <th
                                scope="row"
                                style={{ width: "400px" }}
                                className={"text-capitalize"}
                              >
                                Blockchain
                              </th>
                              <td>Ethereum</td>
                            </tr>
                          </tbody>
                        </Table>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          ) : (
            <Row>
              <Col lg="3">
                <Card>
                  <CardBody>
                    <div>
                      ERROR: invalid ID
                      <br />
                      <br />
                      <Link to={"/list-chain-bots"} className="text-dark">
                        {" "}
                        Back{" "}
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          )}
        </Container>
      </div>
    </React.Fragment>
  )
}

export default ChainBotDetail
