import React, { useState, useEffect } from "react"
import {
  Button,
  Card,
  CardBody,
  CardSubtitle,
  CardTitle,
  Col,
  Container,
  FormGroup,
  Label,
  Row,
} from "reactstrap"
import { useHistory } from "react-router-dom"

import { AvForm, AvField } from "availity-reactstrap-validation"
import Web3 from "web3"
import Robohash from "react-robohash"
import Nftabi from "common/abis/ChainBotzNft.json"

//Import Breadcrumb
import Breadcrumbs from "../../components/Common/Breadcrumb"

const AddChainBot = () => {
  const history = useHistory()
  const [chainbotname, setchainbotname] = useState([])
  const [account, setAccount] = useState([])
  const [contract, setContract] = useState([])

  async function handleSubmit(event, errors, values) {
    if (errors.length == 0) {
      console.log(values)
      try {
        // create a ChainBot NFT
        await contract.methods
          .createChainBot(
            values.chainbotname,
            values.productdesc,
            values.price,
            values.skill
          )
          .send({ from: account })
          .on("receipt", function () {
            history.push("/list-chain-bots")
          })
      } catch (error) {
        // redirect to error
        console.error(error)
      }
    } else {
      console.error(errors)
    }
  }

  function handleChange(data) {
    console.log(data.target.defaultValue)
    setchainbotname("" + data.target.defaultValue)
  }

  useEffect(() => {
    loadWeb3()
    loadContractData()
  }, [])

  // Load the Web3 Provider
  const loadWeb3 = async () => {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
    } else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    } else {
      window.alert("MetaMask not installed")
    }
  }

  // Load the smart contraact Data
  const loadContractData = async () => {
    const web3 = window.web3
    const accounts = await web3.eth.getAccounts()
    setAccount(accounts[0])
    const networkID = await web3.eth.net.getId()
    const networkData = Nftabi.networks[parseInt(networkID)]
    if (networkData) {
      setContract(new web3.eth.Contract(Nftabi.abi, networkData.address))
    } else {
      // redirect to error
    }
  }

  return (
    <React.Fragment>
      <div className="page-content">
        <Container fluid>
          <Breadcrumbs title="Shop" breadcrumbItem="Add Chain Bot" />
          <Row>
            <Col xs="12">
              <Card>
                <CardBody>
                  <CardTitle>Basic Information</CardTitle>
                  <CardSubtitle className="mb-3">
                    Fill all information below
                  </CardSubtitle>

                  <AvForm className="needs-validation" onSubmit={handleSubmit}>
                    <Row>
                      <Col sm="6">
                        <FormGroup>
                          <Label htmlFor="chainbotname1">Chain Bot Name</Label>
                          <AvField
                            id="chainbotname1"
                            name="chainbotname"
                            type="text"
                            className="form-control"
                            onKeyUp={handleChange}
                            required
                            validate={{
                              required: {
                                value: true,
                                errorMessage:
                                  "Please enter a name between 1 and 100 characters",
                              },
                              pattern: {
                                value:
                                  "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$",
                                errorMessage:
                                  "Name can only consist of numbers, letters and spaces",
                              },
                              minLength: {
                                value: 1,
                                errorMessage:
                                  "Name entered is greater then the max 1",
                              },
                              maxLength: {
                                value: 100,
                                errorMessage:
                                  "Name entered is greater then the max 100",
                              },
                            }}
                          />
                        </FormGroup>
                        <FormGroup>
                          <Label htmlFor="skill">Skill</Label>
                          <AvField
                            id="skill"
                            name="skill"
                            type="number"
                            className="form-control"
                            required
                            validate={{
                              required: {
                                value: true,
                                errorMessage:
                                  "Please enter a number between 1 and 5",
                              },
                              pattern: {
                                value: "^[0-9]+$",
                                errorMessage: "Skill can only be numbers",
                              },
                              min: {
                                value: 1,
                                errorMessage:
                                  "Number entered is less then the min 1",
                              },
                              max: {
                                value: 5,
                                errorMessage:
                                  "Number entered is greater then the max 5",
                              },
                            }}
                          />
                        </FormGroup>
                        <FormGroup>
                          <Label htmlFor="price">Price in Eth</Label>
                          <AvField
                            id="price"
                            name="price"
                            type="number"
                            className="form-control"
                            required
                            validate={{
                              required: {
                                value: true,
                                errorMessage:
                                  "Please enter a price in ETH between 1 and 100",
                              },
                              pattern: {
                                value: "^[0-9]+$",
                                errorMessage: "Price can only be numbers",
                              },
                              min: {
                                value: 1,
                                errorMessage:
                                  "Price entered has to be greater then 0",
                              },
                              max: {
                                value: 100,
                                errorMessage:
                                  "Price entered is greater then the max 100",
                              },
                            }}
                          />
                        </FormGroup>
                        <FormGroup>
                          <Label htmlFor="productdesc">
                            Product Description
                          </Label>
                          <AvField
                            className="form-control"
                            id="productdesc"
                            name="productdesc"
                            rows="5"
                            required
                            validate={{
                              required: {
                                value: true,
                                errorMessage:
                                  "Please enter a description between 1 and 2048 characters",
                              },
                              pattern: {
                                value:
                                  "^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$",
                                errorMessage:
                                  "Description can only consist of numbers, letters and spaces",
                              },
                              minLength: {
                                value: 1,
                                errorMessage:
                                  "No description entered is nill, please add add one",
                              },
                              maxLength: {
                                value: 2048,
                                errorMessage:
                                  "Desctiption entered is greater then the max 2048",
                              },
                            }}
                          />
                        </FormGroup>
                      </Col>

                      <Col sm="6">
                        <Robohash
                          className="img-fluid mx-auto d-block"
                          name={chainbotname}
                          type="robot"
                          background="2"
                          required
                        />
                      </Col>
                    </Row>

                    <Button
                      type="submit"
                      color="primary"
                      className="mr-1 waves-effect waves-light"
                    >
                      Save Changes
                    </Button>
                  </AvForm>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

export default AddChainBot
