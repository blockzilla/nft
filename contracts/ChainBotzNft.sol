pragma solidity 0.6.6;

// load the Openzeplin helper contracts
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

// create the ERC271 contract
contract ChainBotzNft is ERC721, Ownable {
    uint256 public tokenCounter;
    string[] public names;
    ERC721 public nftAddress;
    uint256 public currentPrice;
    uint256 public maxNfts =
        115792089237316195423570985008687907853269984665640564039457;

    // struct to hold the bot data
    struct ChainBot {
        uint256 skill;
        uint256 price;
        string name;
        string description;
    }

    // array of the bots
    ChainBot[] public chainBots;
    event Sent(address payable indexed payee, uint256 amount, uint256 balance);
    event Received(
        address indexed payer,
        uint256 tokenId,
        uint256 amount,
        uint256 balance
    );

    mapping(string => bool) _nameExists;

    // initialise the token constructor and token name
    constructor() public ERC721("ChainBotz", "CBZ") {
        tokenCounter = 0;
    }

    // create a bot
    function createChainBot(
        string memory _name,
        string memory _description,
        uint256 price,
        uint256 skill
    ) public returns (uint256) {
        bytes memory tmpname = bytes(_name);
        bytes memory tmpdescription = bytes(_description);

        // make sure the name is unique
        require(!_nameExists[_name], "name already exists");
        require(
            tmpdescription.length > 0 && tmpdescription.length <= 2048,
            "description must be a minimum of 1 charachters and less then the max of 2048"
        );
        require(
            tmpname.length > 0 && tmpname.length <= 100,
            "name must not be empty or greate then 100 characters"
        );
        require(
            skill > 0 && skill <= 5,
            "skill must be a positive number between 0 -> 5"
        );
        require(
            price > 0 && skill <= 100,
            "price must be a positive number between 0 -> 100 ETH"
        );

        // update the token id and counter
        uint256 newItemId = tokenCounter;
        names.push(_name);

        // add to the array of bots
        chainBots.push(ChainBot(skill, price, _name, _description));

        // mint the nft and set the token URI
        _safeMint(msg.sender, newItemId);

        // update our counter and the names list to prevent duplicates
        tokenCounter = tokenCounter + 1;
        _nameExists[_name] = true;

        return newItemId;
    }

    // get the price of the bot nft
    function getPrice(uint256 tokenId) public view returns (uint256) {
        require(tokenId >= 0 && tokenId <= maxNfts, "token id out of range");
        return chainBots[tokenId].price;
    }

    // retuyrn a count of all bots
    function getChainBotCount() public view returns (uint256) {
        return chainBots.length;
    }

    // get a chain bot instance by token id
    function getChainBot(uint256 tokenId)
        public
        view
        returns (
            string memory,
            string memory,
            uint256,
            uint256
        )
    {
        require(tokenId >= 0 && tokenId <= maxNfts, "token id out of range");
        return (
            chainBots[tokenId].name,
            chainBots[tokenId].description,
            chainBots[tokenId].price,
            chainBots[tokenId].skill
        );
    }

    function setCurrentPrice(uint256 _currentPrice) public onlyOwner {
        require(_currentPrice > 0);
        currentPrice = _currentPrice;
    }

    function sendTo(address payable _payee, uint256 _amount) public onlyOwner {
        require(_payee != address(0) && _payee != address(this));
        require(_amount > 0 && _amount <= address(this).balance);
        _payee.transfer(_amount);
        emit Sent(_payee, _amount, address(this).balance);
    }

    function purchaseToken(uint256 _tokenId) public payable {
        require(msg.sender != address(0) && msg.sender != address(this));
        require(msg.value >= currentPrice);
        address tokenSeller = nftAddress.ownerOf(_tokenId);
        nftAddress.safeTransferFrom(tokenSeller, msg.sender, _tokenId);
        emit Received(msg.sender, _tokenId, msg.value, address(this).balance);
    }
}
