var mime_samples = [
  { 'mime': 'application/javascript', 'samples': [
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'dir': '_m0/0', 'linked': 2, 'len': 400000 },
    { 'url': 'http://192.168.1.33:3000/static/js/bundle.js/', 'dir': '_m0/1', 'linked': 2, 'len': 35884 },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'dir': '_m0/2', 'linked': 2, 'len': 400000 },
    { 'url': 'http://192.168.1.33:3000/manifest.json', 'dir': '_m0/3', 'linked': 2, 'len': 517 } ]
  },
  { 'mime': 'application/xhtml+xml', 'samples': [
    { 'url': 'http://192.168.1.33:3000/', 'dir': '_m1/0', 'linked': 2, 'len': 1037 },
    { 'url': 'http://192.168.1.33:3000/fonts/boxicons.svg', 'dir': '_m1/1', 'linked': 1, 'len': 157 },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U/', 'dir': '_m1/2', 'linked': 0, 'len': 1316 } ]
  },
  { 'mime': 'image/png', 'samples': [
    { 'url': 'http://192.168.1.33:3000/logo192.png', 'dir': '_m2/0', 'linked': 2, 'len': 5347 } ]
  },
  { 'mime': 'image/x-ms-bmp', 'samples': [
    { 'url': 'http://192.168.1.33:3000/favicon.ico', 'dir': '_m3/0', 'linked': 2, 'len': 16958 } ]
  },
  { 'mime': 'text/plain', 'samples': [
    { 'url': 'http://192.168.1.33:3000/boxicons.min.css', 'dir': '_m4/0', 'linked': 2, 'len': 63235 },
    { 'url': 'http://192.168.1.33:3000/jquery-3.4.1.slim.min.js', 'dir': '_m4/1', 'linked': 5, 'len': 71037 } ]
  }
];

var issue_samples = [
  { 'severity': 3, 'type': 40401, 'samples': [
    { 'url': 'http://192.168.1.33:3000/jquery-3.4.1.slim.min.js', 'extra': 'Delimited database dump', 'sid': '0', 'dir': '_i0/0' } ]
  },
  { 'severity': 2, 'type': 30701, 'samples': [
    { 'url': 'http://192.168.1.33:3000/boxicons.min.css', 'extra': 'conflicting \x27Cache-Control\x27 data', 'sid': '0', 'dir': '_i1/0' },
    { 'url': 'http://192.168.1.33:3000/favicon.ico', 'extra': 'conflicting \x27Cache-Control\x27 data', 'sid': '0', 'dir': '_i1/1' },
    { 'url': 'http://192.168.1.33:3000/jquery-3.4.1.slim.min.js', 'extra': 'conflicting \x27Cache-Control\x27 data', 'sid': '0', 'dir': '_i1/2' },
    { 'url': 'http://192.168.1.33:3000/logo192.png', 'extra': 'conflicting \x27Cache-Control\x27 data', 'sid': '0', 'dir': '_i1/3' },
    { 'url': 'http://192.168.1.33:3000/manifest.json', 'extra': 'conflicting \x27Cache-Control\x27 data', 'sid': '0', 'dir': '_i1/4' } ]
  },
  { 'severity': 1, 'type': 20101, 'samples': [
    { 'url': 'http://192.168.1.33:3000/fonts/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/0' },
    { 'url': 'http://192.168.1.33:3000/node_modules/css-loader/dist/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/1' },
    { 'url': 'http://192.168.1.33:3000/node_modules/css-loader/dist/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/2' },
    { 'url': 'http://192.168.1.33:3000/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/3' },
    { 'url': 'http://192.168.1.33:3000/node_modules/css-loader/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/adapters/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/5' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.eot/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/6' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.js.woff2/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/7' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@babel/runtime/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/8' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/9' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/address/lib.esm/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/10' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/bignumber/lib.esm/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/11' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/signing-key/lib.esm/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/12' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@hypnosphi/create-react-context/lib/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/13' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/14' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/node_modules/axios/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/15' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/node_modules/axios/lib/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/16' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/node_modules/axios/lib/adapters/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/17' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/node_modules/axios/lib/helpers/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/18' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/hash.js/lib/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/19' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/process/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/20' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/prop-types/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/21' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/22' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/FOO-sfi9876/', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/23' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/css-loader/dist/runtime/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/24' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/adapters/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/25' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/address/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/26' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@hypnosphi/create-react-context/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/27' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/28' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/hash.js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/29' },
    { 'url': 'http://192.168.1.33:3000/static/js/images/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/30' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/src/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/31' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/src/assets/images/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/32' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'During injection testing', 'sid': '0', 'dir': '_i2/33' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/34' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@babel/runtime/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/35' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/address/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/36' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@hypnosphi/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/37' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/38' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'During injection testing', 'sid': '0', 'dir': '_i2/39' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/src/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/40' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/src/assets/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/41' },
    { 'url': 'http://192.168.1.33:3000/static/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/42' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/43' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/44' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/45' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/css-loader/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/46' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/css-loader/dist/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/47' } ]
  },
  { 'severity': 0, 'type': 10901, 'samples': [
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': '', 'sid': '0', 'dir': '_i3/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.js.woff2/', 'extra': '', 'sid': '0', 'dir': '_i3/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/%E6%96%BBY%83U.woff2/', 'extra': '', 'sid': '0', 'dir': '_i3/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': '', 'sid': '0', 'dir': '_i3/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': '', 'sid': '0', 'dir': '_i3/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': '', 'sid': '0', 'dir': '_i3/5' },
    { 'url': 'http://192.168.1.33:3000/jquery-3.4.1.slim.min.js', 'extra': '', 'sid': '0', 'dir': '_i3/6' } ]
  },
  { 'severity': 0, 'type': 10801, 'samples': [
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js//', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/bundle.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js//', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/5' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/6' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/7' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/8' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/9' } ]
  },
  { 'severity': 0, 'type': 10601, 'samples': [
    { 'url': 'http://192.168.1.33:3000/jquery-3.4.1.slim.min.js', 'extra': '', 'sid': '0', 'dir': '_i5/0' } ]
  },
  { 'severity': 0, 'type': 10405, 'samples': [
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js//', 'extra': '', 'sid': '0', 'dir': '_i6/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U/', 'extra': '', 'sid': '0', 'dir': '_i6/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.eot/', 'extra': '', 'sid': '0', 'dir': '_i6/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.js.woff2/', 'extra': '', 'sid': '0', 'dir': '_i6/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/%E6%96%BBY%83U.woff2/', 'extra': '', 'sid': '0', 'dir': '_i6/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js//', 'extra': '', 'sid': '0', 'dir': '_i6/5' } ]
  },
  { 'severity': 0, 'type': 10403, 'samples': [
    { 'url': 'http://192.168.1.33:3000/boxicons.min.css/.htaccess.aspx--\x3e\x22\x3e\x27\x3e\x27\x22\x3csfi000029v677974\x3e', 'extra': '', 'sid': '0', 'dir': '_i7/0' },
    { 'url': 'http://192.168.1.33:3000/favicon.ico/.htaccess.aspx--\x3e\x22\x3e\x27\x3e\x27\x22\x3csfi000005v677974\x3e', 'extra': '', 'sid': '0', 'dir': '_i7/1' },
    { 'url': 'http://192.168.1.33:3000/jquery-3.4.1.slim.min.js/.htaccess.aspx--\x3e\x22\x3e\x27\x3e\x27\x22\x3csfi000009v677974\x3e', 'extra': '', 'sid': '0', 'dir': '_i7/2' },
    { 'url': 'http://192.168.1.33:3000/logo192.png/.htaccess.aspx--\x3e\x22\x3e\x27\x3e\x27\x22\x3csfi000013v677974\x3e', 'extra': '', 'sid': '0', 'dir': '_i7/3' },
    { 'url': 'http://192.168.1.33:3000/manifest.json/.htaccess.aspx--\x3e\x22\x3e\x27\x3e\x27\x22\x3csfi000025v677974\x3e', 'extra': '', 'sid': '0', 'dir': '_i7/4' } ]
  },
  { 'severity': 0, 'type': 10401, 'samples': [
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U/', 'extra': '', 'sid': '0', 'dir': '_i8/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.eot/', 'extra': '', 'sid': '0', 'dir': '_i8/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.js.woff2/', 'extra': '', 'sid': '0', 'dir': '_i8/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/%E6%96%BBY%83U.woff2/', 'extra': '', 'sid': '0', 'dir': '_i8/3' } ]
  },
  { 'severity': 0, 'type': 10205, 'samples': [
    { 'url': 'http://192.168.1.33:3000/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/bundle.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/5' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/6' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/7' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i9/8' } ]
  },
  { 'severity': 0, 'type': 10204, 'samples': [
    { 'url': 'http://192.168.1.33:3000/', 'extra': 'X-Powered-By', 'sid': '0', 'dir': '_i10/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.eot/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/fH%E6Y%83U.js.woff2/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/%E6%96%BBY%83U.woff2/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/5' },
    { 'url': 'http://192.168.1.33:3000/static/js/bundle.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/6' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/7' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/8' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/9' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/10' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/11' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i10/12' } ]
  }
];

