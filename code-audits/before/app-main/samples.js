var mime_samples = [
  { 'mime': 'application/javascript', 'samples': [
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'dir': '_m0/0', 'linked': 2, 'len': 400000 },
    { 'url': 'http://192.168.1.33:3000/static/js/bundle.js/', 'dir': '_m0/1', 'linked': 2, 'len': 35884 },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'dir': '_m0/2', 'linked': 2, 'len': 400000 },
    { 'url': 'http://192.168.1.33:3000/manifest.json', 'dir': '_m0/3', 'linked': 2, 'len': 517 } ]
  },
  { 'mime': 'application/xhtml+xml', 'samples': [
    { 'url': 'http://192.168.1.33:3000/', 'dir': '_m1/0', 'linked': 2, 'len': 1085 },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/css-loader/dist/cjs.js', 'dir': '_m1/1', 'linked': 1, 'len': 198 } ]
  },
  { 'mime': 'image/png', 'samples': [
    { 'url': 'http://192.168.1.33:3000/logo192.png', 'dir': '_m2/0', 'linked': 2, 'len': 5347 } ]
  },
  { 'mime': 'image/x-ms-bmp', 'samples': [
    { 'url': 'http://192.168.1.33:3000/favicon.ico', 'dir': '_m3/0', 'linked': 2, 'len': 16958 } ]
  }
];

var issue_samples = [
  { 'severity': 3, 'type': 40201, 'samples': [
    { 'url': 'http://192.168.1.33:3000/', 'extra': 'https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css', 'sid': '0', 'dir': '_i0/0' },
    { 'url': 'http://192.168.1.33:3000/', 'extra': 'https://code.jquery.com/jquery-3.4.1.slim.min.js', 'sid': '0', 'dir': '_i0/1' } ]
  },
  { 'severity': 2, 'type': 30701, 'samples': [
    { 'url': 'http://192.168.1.33:3000/favicon.ico', 'extra': 'conflicting \x27Cache-Control\x27 data', 'sid': '0', 'dir': '_i1/0' },
    { 'url': 'http://192.168.1.33:3000/logo192.png', 'extra': 'conflicting \x27Cache-Control\x27 data', 'sid': '0', 'dir': '_i1/1' },
    { 'url': 'http://192.168.1.33:3000/manifest.json', 'extra': 'conflicting \x27Cache-Control\x27 data', 'sid': '0', 'dir': '_i1/2' } ]
  },
  { 'severity': 1, 'type': 20101, 'samples': [
    { 'url': 'http://192.168.1.33:3000/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/0' },
    { 'url': 'http://192.168.1.33:3000/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/1' },
    { 'url': 'http://192.168.1.33:3000/node_modules/css-loader/dist/runtime/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/adapters/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/lib/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/5' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/abi/lib.esm/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/6' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/address/lib.esm/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/7' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/keccak256/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/8' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/rlp/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/9' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/signing-key/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/10' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@hypnosphi/create-react-context/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/11' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/bn.js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/12' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/prop-types/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/13' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/webpack/buildin/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/14' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/css-loader/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/15' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/css-loader/dist/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/16' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/src/assets/fonts/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/17' },
    { 'url': 'http://192.168.1.33:3000/static/js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/18' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/19' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/constants/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/20' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/21' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/22' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/webpack/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/23' },
    { 'url': 'http://192.168.1.33:3000/static/js/images/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/24' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/25' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'During injection testing', 'sid': '0', 'dir': '_i2/26' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@babel/runtime/helpers/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/27' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/28' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@ethersproject/bignumber/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/29' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/bn.js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/30' },
    { 'url': 'http://192.168.1.33:3000/static/js/images/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/31' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/32' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/33' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/34' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@hypnosphi/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/35' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/36' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/@openzeppelin/gsn-provider/node_modules/axios/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/37' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/node_modules/js-sha3/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/38' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'During injection testing', 'sid': '0', 'dir': '_i2/39' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/css-loader/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/40' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/node_modules/css-loader/dist/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/41' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/src/assets/FOO-sfi9876', 'extra': 'PUT upload', 'sid': '0', 'dir': '_i2/42' } ]
  },
  { 'severity': 0, 'type': 10901, 'samples': [
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': '', 'sid': '0', 'dir': '_i3/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': '', 'sid': '0', 'dir': '_i3/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': '', 'sid': '0', 'dir': '_i3/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': '', 'sid': '0', 'dir': '_i3/3' } ]
  },
  { 'severity': 0, 'type': 10801, 'samples': [
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/bundle.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/5' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/6' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/7' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'application/javascript', 'sid': '0', 'dir': '_i4/8' } ]
  },
  { 'severity': 0, 'type': 10403, 'samples': [
    { 'url': 'http://192.168.1.33:3000/favicon.ico/.htaccess.aspx--\x3e\x22\x3e\x27\x3e\x27\x22\x3csfi000001v818756\x3e', 'extra': '', 'sid': '0', 'dir': '_i5/0' },
    { 'url': 'http://192.168.1.33:3000/logo192.png/.htaccess.aspx--\x3e\x22\x3e\x27\x3e\x27\x22\x3csfi000005v818756\x3e', 'extra': '', 'sid': '0', 'dir': '_i5/1' },
    { 'url': 'http://192.168.1.33:3000/manifest.json/.htaccess.aspx--\x3e\x22\x3e\x27\x3e\x27\x22\x3csfi000009v818756\x3e', 'extra': '', 'sid': '0', 'dir': '_i5/2' } ]
  },
  { 'severity': 0, 'type': 10205, 'samples': [
    { 'url': 'http://192.168.1.33:3000/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/bundle.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/5' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/6' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/7' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/8' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/sfi9876', 'extra': '', 'sid': '0', 'dir': '_i6/9' } ]
  },
  { 'severity': 0, 'type': 10204, 'samples': [
    { 'url': 'http://192.168.1.33:3000/', 'extra': 'X-Powered-By', 'sid': '0', 'dir': '_i7/0' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/1' },
    { 'url': 'http://192.168.1.33:3000/static/js/bundle.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/2' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/3' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/4' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/5' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/6' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/7' },
    { 'url': 'http://192.168.1.33:3000/static/js/0.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/8' },
    { 'url': 'http://192.168.1.33:3000/static/js/main.chunk.js/', 'extra': 'X-Content-Type-Options', 'sid': '0', 'dir': '_i7/9' } ]
  }
];

