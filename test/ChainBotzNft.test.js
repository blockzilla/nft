const ChainBotzNft = artifacts.require("../contracts/ChainBotzNft.sol")

require("chai").use(require("chai-as-promised")).should()

const longStr =
  "0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d0xb7D930Ba0E5A5069bA4137BD2d16C74667A8579d"

contract("ChainBotzNft", accounts => {
  let contract

  before(async () => {
    contract = await ChainBotzNft.deployed()
  })

  describe("deply test", async () => {
    it("contract deployed correctly", async () => {
      const address = contract.address
      assert.notEqual(address, 0x0)
      assert.notEqual(address, "")
      assert.notEqual(address, null)
      assert.notEqual(address, undefined)
    })

    it("has the ChainBotz name", async () => {
      const name = await contract.name()
      assert.equal(name, "ChainBotz")
    })

    it("has the CBZ symbol", async () => {
      const symbol = await contract.symbol()
      assert.equal(symbol, "CBZ")
    })
  })

  describe("createChainBot", async () => {
    it("creates a chain bot", async () => {
      const result = await contract.createChainBot(
        "Botty%20Rambler",
        "This is a test description",
        1,
        2
      )
      const totalSupply = await contract.totalSupply()

      assert.equal(totalSupply, 1)
      const event = result.logs[0].args

      assert.equal(
        event.from,
        "0x0000000000000000000000000000000000000000",
        "from is correct"
      )
      assert.equal(event.to, accounts[0], "to is correct")
      // check for unique names
      await contract.createChainBot(
        "Botty%20Rambler",
        "This is a test description",
        1,
        2
      ).should.be.rejected
    })
  })

  describe("listing chainbotz", async () => {
    it("lists names", async () => {
      await contract.createChainBot("Felza", "This is a test description", 1, 2)
      await contract.createChainBot("Rudy", "This is a test description", 1, 1)
      await contract.createChainBot("Tod", "This is a test description", 1, 2)

      const totalSupply = await contract.totalSupply()
      let name
      let result = []

      for (var c = 1; c <= totalSupply; c++) {
        name = await contract.names(c - 1)
        result.push(name)
      }

      let expected = ["Botty%20Rambler", "Felza", "Rudy", "Tod"]

      assert.equal(result.join(","), expected.join(","))
    })

    describe("pricing chainbotz", async () => {
      it("checks price", async () => {
        let price = await contract.getPrice(0)
        assert.equal(1, price)
      })
    })

    describe("returns the number of chainbotz", async () => {
      it("counts bots", async () => {
        let count = await contract.getChainBotCount()
        assert.equal(4, count)
      })
    })

    describe("returns a chainbot", async () => {
      it("gets a chainbot", async () => {
        let chainBot = await contract.getChainBot(1)

        console.log(chainBot)
        assert.equal("Felza", chainBot[0])
        assert.equal("This is a test description", chainBot[1])
        assert.equal(1, chainBot[2])
        assert.equal(2, chainBot[3])
      })
    })

    describe("returns validation errors", async () => {
      it("fails token uri > 2048", async () => {
        const result = await contract.createChainBot(
          longStr,
          "test name",
          "test test",
          1,
          1
        ).should.be.rejected
      })

      it("fails token uri < 12", async () => {
        const result = await contract.createChainBot(
          "lessthen12",
          "test name",
          "test test",
          1,
          1
        ).should.be.rejected
      })

      it("fails null name", async () => {
        const result = await contract.createChainBot("", "test test", 1, 1)
          .should.be.rejected
      })

      it("fails name > 100", async () => {
        const result = await contract.createChainBot(longStr, "test", 1, 1)
          .should.be.rejected
      })

      it("fails null description", async () => {
        const result = await contract.createChainBot("test name", "", 1, 1)
          .should.be.rejected
      })

      it("fails description > 2048", async () => {
        const result = await contract.createChainBot("test name", longStr, 1, 1)
          .should.be.rejected
      })

      it("fails 0 stats", async () => {
        const result = await contract.createChainBot("test name", "", 0, 1)
          .should.be.rejected
      })

      it("fails stats > 5", async () => {
        const result = await contract.createChainBot("test name", "", 10, 1)
          .should.be.rejected
      })

      it("fails 0 price", async () => {
        const result = await contract.createChainBot("test name", "", 1, 0)
          .should.be.rejected
      })

      it("fails price > 100", async () => {
        const result = await contract.createChainBot("test name", "", 1, 101)
          .should.be.rejected
      })
    })
  })
})
